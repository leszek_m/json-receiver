package com.networkedassets.jsonreceiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonReceiverApplication {

    public static void main(String[] args) {
        SpringApplication.run(JsonReceiverApplication.class, args);
    }

}
