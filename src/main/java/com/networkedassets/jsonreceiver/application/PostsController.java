package com.networkedassets.jsonreceiver.application;

import com.networkedassets.jsonreceiver.domain.PostsArchiverService;
import com.networkedassets.jsonreceiver.domain.PostsReceiverService;
import com.networkedassets.jsonreceiver.domain.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/posts")
public class PostsController {
    private final PostsReceiverService postsReceiverService;
    private final PostsArchiverService postsArchiverService;

    @Autowired
    public PostsController(PostsArchiverService postsArchiverService, PostsReceiverService postsReceiverService) {
        this.postsReceiverService = postsReceiverService;
        this.postsArchiverService = postsArchiverService;
    }

    @GetMapping("/do-backup")
    public void backupPosts() {
        Collection<Post> posts = postsReceiverService.getAllPosts();
        postsArchiverService.archivePosts(posts);
    }
}
