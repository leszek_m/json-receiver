package com.networkedassets.jsonreceiver.infrastructure;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.networkedassets.jsonreceiver.domain.PostsArchiver;
import com.networkedassets.jsonreceiver.domain.model.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Collection;

@Component
public class FilePostsArchiver implements PostsArchiver {
    private final Logger logger = LoggerFactory.getLogger(FilePostsArchiver.class);

    @Value("${posts.archivePath}")
    private String archivePath;

    @Override
    public void save(Collection<Post> posts) {
        File dir = createDestinationDirectory();
        posts.parallelStream().forEach(post -> {
            savePost(post, dir);
        });
    }

    private File createDestinationDirectory() {
        File dir = new File(archivePath);
        dir.mkdirs();
        return dir;
    }

    private void savePost(Post post, final File directory) {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File(directory.getAbsolutePath() + "/" + post.getId() + ".json");
        try {
            mapper.writeValue(file, post);
        } catch (Exception e) {
            logger.error ("Couldn't write post (id: " + post.getId() + ") to file", e);
        }
    }
}
