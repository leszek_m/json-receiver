package com.networkedassets.jsonreceiver.infrastructure;

import com.networkedassets.jsonreceiver.domain.PostsReceiver;
import com.networkedassets.jsonreceiver.domain.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collection;

@Component
public class RestPostsReceiver implements PostsReceiver {
    @Value("${posts.restPath}")
    private String postsUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public Collection<Post> getAll() {
        ResponseEntity<Post[]> response = restTemplate.getForEntity(postsUrl, Post[].class);
        return Arrays.asList(response.getBody());
    }
}
