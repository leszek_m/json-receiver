package com.networkedassets.jsonreceiver.domain;

import com.networkedassets.jsonreceiver.domain.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PostsReceiverService {
    private final PostsReceiver receiver;

    @Autowired
    public PostsReceiverService(PostsReceiver postsReceiver) {
        receiver = postsReceiver;
    }

    public Collection<Post> getAllPosts() {
        return receiver.getAll();
    }
}
