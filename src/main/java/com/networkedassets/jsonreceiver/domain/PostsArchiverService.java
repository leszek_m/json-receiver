package com.networkedassets.jsonreceiver.domain;

import com.networkedassets.jsonreceiver.domain.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class PostsArchiverService {
    private final PostsArchiver archiver;

    @Autowired
    PostsArchiverService(PostsArchiver postsArchiver) {
        archiver = postsArchiver;
    }

    public void archivePosts(final Collection<Post> posts) {
        archiver.save(posts);
    }
}
