package com.networkedassets.jsonreceiver.domain.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Post {
    private Long id;
    private Long userId;
    private String title;
    private String body;
}
