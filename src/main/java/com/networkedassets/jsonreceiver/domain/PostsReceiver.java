package com.networkedassets.jsonreceiver.domain;

import com.networkedassets.jsonreceiver.domain.model.Post;

import java.util.Collection;

public interface PostsReceiver {
    Collection<Post> getAll();
}
