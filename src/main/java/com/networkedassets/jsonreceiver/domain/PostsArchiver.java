package com.networkedassets.jsonreceiver.domain;

import com.networkedassets.jsonreceiver.domain.model.Post;

import java.util.Collection;

public interface PostsArchiver {
    void save(final Collection<Post> posts);
}
